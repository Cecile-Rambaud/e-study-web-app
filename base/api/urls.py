from django.urls import path
from . import views
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

urlpatterns = [
    path('',  views.getRoutes),
    path('rooms/', views.getRooms),
    path('rooms/<str:pk>/', views.getRoom)
]